.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

System Requirements
===================

Minimum Requirements
--------------------
We don't know yet. The oldest machine tested on was T400 and it was a bit laggy. If you have any experiences with older machines please let us know.

Recommended Requirements
------------------------
We recommend running a fairly modern machine.

Audio Interface
---------------
An Audio Interface is recommended as it offers low latency and better quality (especially if recording audio).

JACK
----
JACK needs to be set up and configured before running Zrythm. We recommend using `Cadence <http://kxstudio.linuxaudio.org/Applications%3ACadence>`_, as it makes the process very easy.

* Open Cadence

.. image:: /_static/img/cadence.png

* Click Configure and select your Audio Interface

.. image:: /_static/img/cadence-driver.png
