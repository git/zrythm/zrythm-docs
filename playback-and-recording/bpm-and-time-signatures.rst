.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

BPM and Time Signatures
=======================

BPM
---

The BPM can be set to between 40 and 360 by
clicking and dragging the BPM widget. Clicking
and dragging on the integer counterpart will
change the integer (e.g., from 140.24 to
141.24) and clicking and dragging on the
decimal counterpart will change the decimal
value (e.g., frrom 140.24 to 140.25).

Time Signature
--------------

The Time Signature is split into the following

Beats per Bar
  This is the top number that indicates the
  number of Beats that should be in a Bar. It
  can be set from 1 to 16.
Beat Unit
  This is the bottom number that indicates
  the beat unit to be used. It can be set to
  2, 4, 8 or 16.
