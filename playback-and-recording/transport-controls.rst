.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Transport Controls
==================

Playback is mainly controlled by the following
Transport Controls, found in the main toolbar.

Record
  Arms the project for recording.
Play
  If stopped, the song will start playing. If
  already playing, the Playhead will move to
  the Cue point.
Stop
  Pauses playback. If clicked twice, goes
  back to the Cue point.
Backward
  Moves the Playhead backward by the size of
  1 snap point.
Forward
  Moves the Playhead forward by the size of
  1 snap point.
Loop
  If enabled, the Playhead will move back to
  the Loop Start point when it reaches the
  Loop End point.

Metronome
---------

TODO
