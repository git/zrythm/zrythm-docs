.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Loop Points and Markers
=======================

Loop Points
-----------

TODO

Cue Point
---------

The Cue Point is where the Playhead will move
to when Play is clicked while playing or when
Stop is clicked while paused.


Song Start Marker
-----------------

The Song Start marker signifies the position
in the Timeline where the Song will start.
This is mainly used for exporting the mixdown
into audio.

Song End Marker
---------------

The Song End marker signifies the position
in the Timeline where the Song will end.
Likewise, it is mainly used for exporting the
mixdown into audio.

Custom Markers
--------------

Custom Markers can be specified in the Marker
Track. TODO
