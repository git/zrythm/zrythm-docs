.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Zrythm Interface Overview
=========================

Zrythm's interface is split into various sub-modules:

.. image:: /_static/img/ui_annotated_general.png

Inspector Panel (1)
  The inspector panel contains the inspector, which is used to view and change parameters of the currently selected objects.

Editor Panel (2)
  The editor panel contains various views that are useful in composing and mixing, such as the Clip Editor and the Mixer.

Browser Panel (3)
  The browser panel contains the browser, and is used to find plugins and/or audio and MIDI files to drag and drop into the project.

Arranger Panel (4)
  This is where the action happens. The main panel mainly consists of the Timeline Arranger and the Project's Tracks on the left side.

Toolbar (5)
  There are two main toolbars containing global controls such as BPM and Transport.

Title Bar (6)
  The Title Bar contains menus with options for various operations.

Status Bar (7)
  The Status Bar is a helpful bar in the bottom of the program that shows tips based on the currently hovered-over item.
