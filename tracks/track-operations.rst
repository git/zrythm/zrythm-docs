.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Track Operations
================

Moving Tracks
-------------
Tracks can be moved by clicking and dragging
inside empty space in the Track, and dropping
it at another location. The drop locations will
be highlighted as you move the Track.

Deleting Tracks
---------------

Tracks can be deleted by right-clicking them and
selecting :zbutton:`Delete`.

.. tip::
  All Track operations are undoable.
