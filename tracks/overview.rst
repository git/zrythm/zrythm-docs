.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Overview
========

Tracks are the main building blocks of projects.
Tracks appear in the Tracklists (one pinned at
the top and one non-pinned) and contain various
information such as regions and automation
points.

Most types of Tracks have a Channel that
appears in the mixer. Each Track has its
own page in the Inspector section, which is
shown when selecting a Track.

There are various kinds of Tracks suited for
different purposes, explained in the following
sections.
