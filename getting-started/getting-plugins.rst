.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Getting Plugins
===============

Until Zrythm ships with its own bundled plugins, you must install some plugins like synthesizers and effects before you can make music.

Fortunately, there are many plugins to choose from. This guide will show you a few ways you can install plugins.

If you are on Debian/Ubuntu, we highly recommend checking out the
`KXstudio repositories <https://kx.studio/>`_, since there are many
plugins that can easily be installed via apt.

If you are on Arch GNU/Linux you're in luck, because you can just download
the entire `pro-audio <https://www.archlinux.org/groups/x86_64/pro-audio/>`_
or `lv2-plugins <https://www.archlinux.org/groups/x86_64/lv2-plugins/>`_
groups, or you can just hand-pick the plugins you want from there.

If you are not sure where to start, please read on.

Recommended Plugin Bundles
--------------------------

`DISTRHO Ports <https://distrho.sourceforge.io/ports.php>`_ provides a good variety
of synths and a few effects that can get you started for music making
right away.

`ZAM Plugins <http://www.zamaudio.com/?p=976>`_ is an effects pack that contains all the
basic effects like Delay, Reverb, Compression, EQ, Saturation, etc.

`LSP <https://lsp-plug.in/>`_ has a lot of useful effects.

`OpenAV <http://openavproductions.com>`_ has some nice plugins worth checking out.

You might also want to check out `X42 Plugins <http://x42-plugins.com/x42/>`_, which has
A LOT of MIDI effect and meter plugins.

Synths
------

`Helm <https://tytel.org/helm/>`_ is an excellent subtractive synth that should be on
everyone's toolbox.

`ZynFusion <http://zynaddsubfx.sourceforge.net/zyn-fusion.html>`_, the alternative
front-end to ZynAddSubFX is one of the most
powerful synths in existance.
