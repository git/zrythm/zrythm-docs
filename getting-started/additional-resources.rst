.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Additional Resources
====================

PeerTube
--------
Zrythm has a `PeerTube instance <https://video.zrythm.org>`_ open for all GNU/Linux audio users, regardless of their DAW of choice.

Forum
-----
If this manual is not enough, feel free to post on the `official forums`_

.. _official forums: https://forum.zrythm.org

IRC/Matrix
----------
We offer support on IRC at `#zrythm on Freenode <webchat.freenode.net/?channels=zrythm>`_. Matrix users can use `#freenode_#zrythm:matrix.org <https://riot.im/#/room/#freenode_#zrythm:matrix.org>`_.

Issue Tracker
-------------
The issue tracker can be found at `https://git.zrythm.org/zrythm/zrythm/issues <https://git.zrythm.org/zrythm/zrythm/issues>`_.

Source Code
-----------
Zrythm's source code can be found at `https://git.zrythm.org/zrythm/zrythm <https://git.zrythm.org/zrythm/zrythm>`_ or in the mirror repository at `GNU Savannah <http://git.savannah.nongnu.org/cgit/zrythm.git>`_.
