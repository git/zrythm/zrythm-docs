.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Panning
=======

Channels can be panned Left to Right by dragging the Pan slider.
Zrythm supports Linear Pan, Square Root Pan and Sine Law Pan, with
a Pan Law of -6dB, -3dB, or 0dB.

These settings are configurable through the Preferences window. The
default is Sine Law with -3dB. If you don't understand what these mean,
it's best to leave them to their default values.
