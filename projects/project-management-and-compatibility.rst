.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Project Management and Compatibility
====================================

For at least the time being, Zrythm follows a rolling release model and
project file structure is subject to change at each new release. No
compatibility will be maintained between version changes in order to
give more time to feature development and fixes.

It may be possible to open projects from previous versions if there was
no change in the project file structure, but this is not guaranteed,
so if you would like to open a project made using a previous version
please install that version of Zrythm.

.. hint::
  Each project file will contain the version of Zrythm it was made with
