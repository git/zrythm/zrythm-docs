.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Zrythm Manual
=============

Welcome to the Zrythm manual. Please follow the sections below to get started.

.. toctree::
   :maxdepth: 1

   getting-started/intro
   configuration/intro
   zrythm-interface/intro
   projects/intro
   plugins-files/intro
   tracks/intro
   chords-and-scales/intro
   editing/intro
   mixing/intro
   playback-and-recording/intro
   automation/intro
   modulators/intro
   exporting/intro
   publishing/intro
   contributing/intro
   appendix/intro
