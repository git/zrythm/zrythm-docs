# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2019, Alexandros Theodotou
# This file is distributed under the same license as the Zrythm package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Zrythm 0.5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-12 12:26+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"

#: ../../plugins-files/audio-midi-files/file-browser.rst:2
msgid "File Browser"
msgstr ""

#: ../../plugins-files/audio-midi-files/file-browser.rst:4
msgid ""
"The File Browser makes it easy to navigate through files on your computer"
" or through your custom collections."
msgstr ""

#: ../../plugins-files/audio-midi-files/file-browser.rst:9
msgid "Filtering"
msgstr ""

#: ../../plugins-files/audio-midi-files/file-browser.rst:11
#: ../../plugins-files/audio-midi-files/file-browser.rst:16
#: ../../plugins-files/overview.rst:4
#: ../../plugins-files/plugins/inspector-page.rst:8
#: ../../plugins-files/plugins/plugin-browser.rst:7
#: ../../plugins-files/plugins/plugin-window.rst:8
msgid "TODO"
msgstr ""

#: ../../plugins-files/audio-midi-files/file-browser.rst:14
msgid "Collections"
msgstr ""

#: ../../plugins-files/audio-midi-files/file-browser.rst:19
msgid "Importing Files"
msgstr ""

#: ../../plugins-files/audio-midi-files/file-browser.rst:21
msgid ""
"Files are imported by either double clicking them in the browser or "
"dragging and dropping them into a track."
msgstr ""

#: ../../plugins-files/audio-midi-files/intro.rst:2
msgid "Audio & MIDI Files"
msgstr ""

#: ../../plugins-files/audio-midi-files/overview.rst:2
#: ../../plugins-files/overview.rst:2
msgid "Overview"
msgstr ""

#: ../../plugins-files/audio-midi-files/overview.rst:4
msgid ""
"Zrythm can import MIDI and audio files into the project. The files can be"
" imported by dragging and dropping from your computer or from the File "
"Browser into a track."
msgstr ""

#: ../../plugins-files/audio-midi-files/overview.rst:10
msgid "Supported Audio Formats"
msgstr ""

#: ../../plugins-files/audio-midi-files/overview.rst:11
msgid "Zrythm supports OGG, FLAC, WAV and MP3."
msgstr ""

#: ../../plugins-files/intro.rst:2
msgid "Plugins & Files"
msgstr ""

#: ../../plugins-files/plugins/inspector-page.rst:2
msgid "Inspector Page"
msgstr ""

#: ../../plugins-files/plugins/inspector-page.rst:4
msgid ""
"When a Plugin is selected in the Mixer, its page will appear in the "
"Inspector, like the following."
msgstr ""

#: ../../plugins-files/plugins/inspector-page.rst:10
msgid ""
"This page will display information about the Plugin and allow you to "
"route inputs and outputs to each port of the Plugin, for example to route"
" an LFO output to a Filter Plugin's filter cutoff parameter."
msgstr ""

#: ../../plugins-files/plugins/intro.rst:2
msgid "Plugins"
msgstr ""

#: ../../plugins-files/plugins/plugin-browser.rst:2
msgid "Plugin Browser"
msgstr ""

#: ../../plugins-files/plugins/plugin-browser.rst:4
msgid ""
"The Plugin Browser makes it easy to browse and filter Plugins installed "
"on your computer."
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:2
msgid "Plugin Types"
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:4
msgid "There are three types of Plugins in Zrythm."
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:7
msgid "Instruments"
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:7
msgid ""
"Instruments are Plugins that are used to generate sound, such as synths "
"or pianos."
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:11
msgid "Effects"
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:10
msgid ""
"Effects are Plugins that change the audio signal passed to them. Examples"
" are Reverb, Chorus and Flanger."
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:16
msgid "Modulators"
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:14
msgid ""
"Modulators are used to modulate the parameters of other Plugins. These "
"include LFOs and envelopes."
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:19
msgid "Supported Formats"
msgstr ""

#: ../../plugins-files/plugins/plugin-types.rst:21
msgid "Zrythm supports LV2 Plugins at the moment."
msgstr ""

#: ../../plugins-files/plugins/plugin-window.rst:2
msgid "Plugin Window"
msgstr ""

#: ../../plugins-files/plugins/plugin-window.rst:4
msgid ""
"When Plugin UIs are opened, a window such as the following will be "
"displayed, if the plugin ships with its own UI."
msgstr ""

#: ../../plugins-files/plugins/plugin-window.rst:10
msgid ""
"If the plugin does not ship with its own UI, the following generic UI "
"will be generated for it."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:2
msgid "Ports"
msgstr ""

#: ../../plugins-files/plugins/ports.rst:4
msgid ""
"Plugins expose Ports that are used internally to route MIDI and audio "
"signals to/from and externally for automation."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:8
msgid ""
"A Port can only be an Input Port or an Output Port and can have one of "
"the following types."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:14
msgid "Audio"
msgstr ""

#: ../../plugins-files/plugins/ports.rst:12
msgid ""
"Ports of this type receive or send raw audio signals. Usually, Effect "
"Plugins will have at least two of these as inputs for Left and Right, and"
" at least two as outputs."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:18
msgid "Event"
msgstr ""

#: ../../plugins-files/plugins/ports.rst:17
msgid ""
"Event Ports are mainly used for routing MIDI signals. Instrument Plugins "
"will have at least one Event Port."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:22
msgid "Control"
msgstr ""

#: ../../plugins-files/plugins/ports.rst:21
msgid ""
"Control Ports are Plugin parameters that are usually shown inside the "
"Plugin's UI. These can be automated in automation lanes."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:30
msgid "CV"
msgstr ""

#: ../../plugins-files/plugins/ports.rst:25
msgid ""
"CV Ports are continuous signals that can be fed into or emitted from "
"Plugins, and are mainly used by the Modulators. Each Modulator will have "
"at least one CV output Port which can be routed to Plugin Control Ports "
"for automation."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:32
msgid ""
"Usually, only Ports of the same type can be connected, with the exception"
" of CV ports. CV output Ports may be routed to both CV input Ports and "
"Control input Ports."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:37
msgid "Output Ports may only be routed to Input Ports and vice versa."
msgstr ""

#: ../../plugins-files/plugins/ports.rst:41
msgid ""
"Channels also have their own Ports, for example for the Fader, Pan, and "
"Enabled (On/Off)."
msgstr ""

