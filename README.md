# zrythm-docs

Documentation generator for https://docs.zrythm.org

For bundling for the website, use `make bundle` and it will create a
deployable `_rendered` folder. Just rsync that and done.

# Compiling PDFs

PDF compilation requires texlive. See https://tug.org/texlive/quickinstall.html

# LICENSE
The sources required to build the manual and the contents of the manual are licensed under the AGPLv3+. See the LICENSE
file for more info.

----

Copyright (C) 2019 Alexandros Theodotou

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
