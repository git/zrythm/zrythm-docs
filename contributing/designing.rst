.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Designing
=========

If you want to help improve the appearance Zrythm itself and its website,
forum, manual, etc., please come join the
`chat <https://riot.im/app/#/room/#freenode_#zrythm:matrix.org?action=chat>`_.

Zrythm itself is fully CSS-themable, and the overall UI structure can easily be edited in Glade without touching any code.
