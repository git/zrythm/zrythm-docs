.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Testing
=======

You can fetch the latest master branch from https://git.zrythm.org/zrythm/zrythm and start testing the latest features. You can report any bugs, ideas and impressions by creating an issue there.

If you are on Arch Linux or Debian, the latest master branch can easily be installed via the zrythm-git package on AUR or by installing an auto-generated .deb file. More information at `Installation Instructions <https://git.zrythm.org/zrythm/zrythm#installation>`_.

Manual installation instructions can be found `here <https://git.zrythm.org/zrythm/zrythm/blob/master/INSTALL.md>`_.
