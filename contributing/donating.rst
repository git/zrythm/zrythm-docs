.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Donating
========

Donations are vital to keep the project running smoothly. If you can afford to do
so please consider becoming a patron or
supporting us below.

`PayPal <https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LZWVK6228PQGE&source=url>`_
  PayPal recurring or non-recurring donation.
`LiberaPay <https://liberapay.com/Zrythm>`_
  LiberaPay is a recurring donations platform for funding developers and creators, ran by a French non-profit.
Bitcoin
  Anonoymous cryptocurrency donation. Please use: bc1qjfyu2ruyfwv3r6u4hf2nvdh900djep2dlk746j
