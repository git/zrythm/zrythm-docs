.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Translating
===========

Zrythm is available for translation at Hosted Weblate. Visit the `Zrythm project page <https://hosted.weblate.org/engage/zrythm/?utm_source=widget>`_ to start translating.

The Zrythm translation project contains the following components:

Zrythm
  The actual Zrythm program

website
  The Zrythm website (https://www.zrythm.org)

Manual - *
  Sections of this manual

Click on the project you wish to work on, and then select a language in
the screen that follows. For more information on using Weblate,
please see the `official documentation <https://docs.weblate.org/en/latest/user/translating.html>`_
of Weblate.
