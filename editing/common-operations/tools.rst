.. Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>

   This file is part of Zrythm

   Zrythm is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   Zrythm is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Affero Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Tools
=====

Toolbox
-------
The Toolbox contains the following Tools that
are used to trigger modes for
specific operations.

Selection Tool
  The Selection Tool is the smartest Tool and
  while its main functionality is to select
  and move objects, it can also create or edit
  objects in various ways.
Edit Tool
  Also known as the Pencil Tool, this Tool is
  used to create objects by single-clicking
  and dragging. In the drum view of the Piano
  Roll, it can be used to quickly generate
  multiple hits.
Erase Tool
  The Erase Tool is used to delete all objects
  that are selected by it.

Each tool can be selected
by simply pressing its corresponding button
from 1 to 5 on the keyboard.
Each Mode triggered by each tool is further
described below.

Select Mode
-----------

To select objects in Select Mode, click
and drag to create a selection rectangle.

To create objects in Select Mode, double click
and drag within a track or lane.

To move selected objects, click on one of them
and hold while moving your cursor. If the
object is not selected, it will become selected.

To copy-move (duplicate and move) objects,
click and drag like when moving, while holding
the :zbutton:`Ctrl` button on the keyboard.

To split/cut objects in parts, hold
:zbutton:`Ctrl` while clicking somewhere
inside the object to cut at that position.

Edit Mode
---------

TODO
